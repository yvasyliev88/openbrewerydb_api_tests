# Open Brewery DB API Automated Tests: https://www.openbrewerydb.org/

# Task 1: Cover "Search Breweries" method of https://www.openbrewerydb.org/documentation with autotests using Java + REST Assured

# Pre-condtions (for Mac users): 
- Install maven: ```brew install maven```
- Install allure: ```brew install allure```

# Run Tests
```mvn clean test -Dbrewerydb.baseUrl=https://api.openbrewerydb.org```

# Generate Allure Report
```allure serve path/to/allure/result/folder```

# Remaining scenarios to cover
- Verify JSON schema of response (all required fields, data types, nullable values etc.)
- Verify some response data by comparing with corresponded DB data, like values of 'longitude' & 'latitude' fields
- Verify search results by other criteria:
    - state
    - city
    - postal_code
    - street
    - phone
    - brevery_type

# Task 2: Examine "List Breweries" method and share your thoughts (in readme file) on how you would apply test automation to it

1. The main goal is to make sure that different filters (like by_city, by_name etc.) apply properly and response returns filtered data
-> This can be achieved by comparing actual response data with with DB data

2. The other goal is to make sure that pagination works as expected (default or custom 'per page' and 'Max per page' values, the offset value for pages etc.)
-> Different test design techniques can be used, like boundary values and/or equivalent partition to reduce number of checks

3. The other goal is to make sure that sorting of response data by different criteria (name, type) and different order (DESC and ASC) is also works as expected
-> Pairwise test design techique can be used to create diffrent test data compinations, i.e "DESC sorting by name" or "ASC sorting ny name and type" etc.