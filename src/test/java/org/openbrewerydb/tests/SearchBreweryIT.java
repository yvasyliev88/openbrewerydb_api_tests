package org.openbrewerydb.tests;

import io.qameta.allure.Feature;
import io.restassured.mapper.ObjectMapperType;
import org.openbrewerydb.models.Brewery;
import org.openbrewerydb.tests.base.BaseApiTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;


@Feature("API: Search Brewery")
public class SearchBreweryIT extends BaseApiTest {

    public static final String BREWERIES_SEARCH_URI = "/breweries/search";

    @Test(description = "Search brewery by phrase", groups = { "smoke", "regression" })
    public void searchBreweryByPhrase() {

        String searchCriteria = "brewpub";

        Brewery[] breweriesList = given()
                    .spec(reqSpec)
                    .queryParam("query", searchCriteria)
                .when()
                    .get(BREWERIES_SEARCH_URI)
                .then()
                    .log().ifValidationFails()
                    .spec(resSpec)
                .and()
                    .extract()
                .as(Brewery[].class, ObjectMapperType.GSON);

                assertThat("", breweriesList.length, greaterThan(0));

                assertThat("", Arrays.asList(breweriesList),
                                hasItem(anyOf(
                                        hasProperty("name", containsString(searchCriteria)),
                                        hasProperty("websiteUrl", containsString(searchCriteria))
                                )));
    }

    @Test(description = "Search brewery by exact name", groups = { "smoke", "regression" })
    public void searchBreweryByExactName() {

        String searchCriteria = "Glacier Brewhouse";

        Brewery[] breweriesList = given()
                    .spec(reqSpec)
                    .queryParam("query", searchCriteria)
                .when()
                    .get(BREWERIES_SEARCH_URI)
                .then()
                    .log().ifValidationFails()
                    .spec(resSpec)
                .and()
                    .extract()
                    .as(Brewery[].class, ObjectMapperType.GSON);
        //Type type = new TypeToken<List<Artwork>>(){}.getType();

        assertThat("", breweriesList.length, is(1));

        Arrays.asList(breweriesList).forEach(item ->
                assertThat("", item.getName(), equalTo(searchCriteria))
        );
    }

    @Test(dataProvider = "non existed search criteria", description = "Search brewery by non existed criteria", groups = { "smoke", "regression" })
    public void searchBreweryByNonExistedSearchCriteria(String searchCriteria, int expectedStatusCode) {

        int expectedListSize = given()
                    .spec(reqSpec)
                    .queryParam("query", searchCriteria)
                .when()
                    .get(BREWERIES_SEARCH_URI)
                .then()
                    .log().ifValidationFails()
                    .statusCode(expectedStatusCode)
                .and()
                    .extract()
                    .jsonPath()
                    .getList("$").size();

        assertThat("", expectedListSize, is(0));
    }

    @DataProvider(name = "non existed search criteria")
    public static Object[][] testData(){
        return new Object[][] {
                {"hdsajkhdjksahdjksahdkjhasdsamdbn", 200},
                {"", 200}
        };
    }
}