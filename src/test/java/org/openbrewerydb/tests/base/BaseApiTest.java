package org.openbrewerydb.tests.base;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.openbrewerydb.listeners.ApiTestListener;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

@Listeners({ApiTestListener.class})
public class BaseApiTest {

    protected static FilterableRequestSpecification reqSpec;

    protected static ResponseSpecification resSpec;

    @BeforeSuite(alwaysRun = true)
    public static void preconditions() {
        RestAssured.baseURI = System.getProperty("brewerydb.baseUrl");
        reqSpec = (FilterableRequestSpecification) given()
                .log().uri().contentType(ContentType.JSON).filter(new AllureRestAssured())
                .config(RestAssured.config().objectMapperConfig(new ObjectMapperConfig(ObjectMapperType.GSON)));
        ResponseSpecBuilder res = new ResponseSpecBuilder();
        res.expectStatusCode(200);
        res.expectContentType(ContentType.JSON);
        res.expectBody(".", allOf(notNullValue(), not("")));
        resSpec = res.build();
    }

    @AfterSuite(alwaysRun = true)
    public static void postconditions() {
        RestAssured.reset();
    }
}
