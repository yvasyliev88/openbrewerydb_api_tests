package org.openbrewerydb.listeners;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openbrewerydb.utils.LogUtils;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.IOException;

import static java.lang.System.getProperty;
import static java.util.Optional.ofNullable;

public class ApiTestListener extends TestListenerAdapter {

    @Override
    public void onTestStart(ITestResult result) {
        ofNullable(getProperty("setelAPI.baseUrl")).ifPresent(s -> System.out.println("Env base URL: " + s));
        LogUtils.getInstance().info("Test class started: " + result.getTestClass().getName());
        LogUtils.getInstance().info("Test started: " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        LogUtils.getInstance().info("Test SUCCESS: " + result.getName());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LogUtils.getInstance().info("Test FAILED: " + result.getName());
        if (result.getThrowable()!=null) {
            result.getThrowable().printStackTrace();
        }
    }

    @Attachment(value = "TEST HTML LOG", type = "text/plain")
    private byte[] appendLogToAllure(File file) {
        try {
            return FileUtils.readFileToByteArray(file);
        } catch (IOException ignored) {
        }
        return null;
    }
}
