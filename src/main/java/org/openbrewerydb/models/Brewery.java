package org.openbrewerydb.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Brewery {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("brewery_type")
    @Expose
    private String breweryType;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("address_2")
    @Expose
    private Object address2;
    @SerializedName("address_3")
    @Expose
    private Object address3;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("county_province")
    @Expose
    private Object countyProvince;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("website_url")
    @Expose
    private String websiteUrl;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
}
